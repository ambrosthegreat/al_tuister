#!/usr/bin/env python3
#
# mi_ficheros
# Librería con funciones de ficheros de texto
#

# LIBRERIAS
from os import getcwd
from os import remove
from os.path import isfile as existe_fichero

# FUNCIONES
class mi_ficheros():
    def __init__( self, nom_fich, trayectoria = '/' ):
        if trayectoria[0] != '/':
            trayectoria = '/' + trayectoria
        self.nombre = getcwd() + trayectoria + nom_fich.split( '/' )[-1]

    def en_path( self ):
        # Devuelve el nombre de fichero con la trayectoria actual delante
        return self.nombre

    def en_raiz( self ):
        # Devuelve el nombre de fichero con la barra delante
        return '/' + self.nombre.split( '/' )[-1]

    def existe( self ):
        return existe_fichero( self.nombre )

    def primera_linea( self ):
        # Devuelve la primera línea de un fichero de texto
        # Asumo que se puede abrir el fichero y no haber nada o que se ha podido escribir 'None' como texto
        if self.existe():
            fichero = open( self.nombre, 'r' )
            salida = fichero.read()
            fichero.close()
            if salida != None:
                if salida != 'None':
                    salida = int( salida )
                else:
                    salida = None
            else:
                salida = None
        else:
            salida = None
        return salida

    def escribe_una_linea( self, linea ):
        # Graba un fichero con sólo una línea
        f = open( self.nombre, 'w' )
        f.write( str( linea ) )
        f.close()

    def lee_lineas( self ):
        # Devuelve una lista con cada una de las líneas del fichero sin repetir ninguna y sin espacios delante o detrás
        f = open( self.nombre, 'r' )
        lineas = f.readlines()
        f.close()
    
        salida = []
        for linea in lineas:
            linea = linea.strip()
            if not( linea in salida ):
                salida.append( linea )
    
        return salida

    def borrar( self ):
        # Borra el fichero
        if self.existe():
            remove( self.nombre )

    def escribe_lista( self, lista ):
        # Vuelca una lista a un fichero de texto
        num_elem = len( lista )
        i = 1
        f = open( self.nombre, 'w' )
        for elemento in lista:
            if i < num_elem:
                f.write( str( elemento ) + '\n' )
            else:
                f.write( str( elemento ) )
            i += 1
        f.close()
