#!/usr/bin/env python3
#
# al_tuister.py
#
# Librería para leer mensajes de Mastodon y reenviarlos a Twitter
# Subo a dropbox los temporales
#
# Documentación: https://realpython.com/twitter-bot-python-tweepy/
# Subir imágenes desde una URL: https://teratail.com/questions/195943
# Mogollón de ejemplos: https://www.programcreek.com/python/example/6809/tweepy.API

# CONSTANTES
MI_PATH = 'ficheros_frases/'
EVITAR_DOBLE = 'ult_toot_leido.nfo'
ULTIMOS_TOOTS = 'ult_toots_tuiteados.nfo'
TOOTS_GUARDAR = 20
NUM_TOOTS = 10 # Número de toots a leer cada vez
SENAL_HILO = '#ht'
SENAL_TUIT = '#tt'
SENAL_RTOOT = '#rtt'
SENAL_RHILO = '#rht'
CADRT = '\n\n🔗🐘: '

# LIBRERIAS
from argparse import ArgumentParser
from mi_dropbox import mi_dropbox
from mi_ficheros import mi_ficheros
from mi_mastodon import mi_masto
from mi_tuiter import mi_tuiter

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Tuitear cosas de Mastodon' )
    parser.add_argument( '-d', '--hilo', help='Tuitea el hilo superior a un toot pasado como URL', type = str )
    parser.add_argument( '-db', '--dropbox_token', help='Token de la app de dropbox para los ficheros temporales', type = str )
    parser.add_argument( '-mi', '--masto_inst', help='Instancia de Mastodon', type = str )
    parser.add_argument( '-mt', '--masto_token', help='Token de la app de Mastodon', type = str )
    parser.add_argument( '-t', '--toot', help='Tuitea un toot pasado como URL', type = str )
    parser.add_argument( '-ta', '--twitter_api', help='Twitter API key', type = str )
    parser.add_argument( '-tsa', '--twitter_sapi', help='Twitter secret API key', type = str )
    parser.add_argument( '-tt', '--twitter_token', help='Twitter access token', type = str )
    parser.add_argument( '-tst', '--twitter_stoken', help='Twitter secret access token', type = str )
    return parser.parse_args()

def tuitear_toot( masto, tuiter, url_toot ):
    # Tuiteat un toot
    id_toot = masto.buscar_toot( url_toot )
    tuiter.tuitear( masto.cuerpo( id_toot ), adjuntos = masto.adjuntos( id_toot ) )

def tuitear_hilo( masto, tuiter, url_ultimo ):
    # Tuitea un hilo correspondiente a los mensajes por encima de un toot
    id_ultimo = masto.buscar_toot( url_ultimo )
    hilo_ids = masto.hilo_hacia_arriba( id_ultimo )
    hilo = []
    for id_toot in hilo_ids:
        tuit = {
                'cuerpo' : masto.cuerpo( id_toot ),
                'adjuntos' : masto.adjuntos( id_toot )
                }
        hilo.append( tuit )
    tuiter.tuit_hilo( hilo )

def quita_repes( lista ):
    salida = []
    for elem in lista:
        if not( elem in salida ):
            salida.append( elem )
    return salida

def guarda_ultimos( lista_tuits, dbx ):
    # Guarda en dropbox los últimos toots enviados a twitter
    lista_tuits = quita_repes( lista_tuits )
    inicial = len( lista_tuits ) + 1 - TOOTS_GUARDAR
    if inicial < 1:
        inicial = 1
    lista_guardar = []
    conteo = 0
    for id_toot in lista_tuits:
        conteo += 1
        if conteo >= inicial:
            lista_guardar.append( id_toot )

    f_tuits = mi_ficheros( ULTIMOS_TOOTS, MI_PATH )
    f_tuits.escribe_lista( lista_guardar )
    dbx.cargar( f_tuits.en_path(), f_tuits.en_raiz() )

def guarda_el_ultimo( texto, dbx ):
    # Guarda el dropbox el último toot procesado
    f_ult = mi_ficheros( EVITAR_DOBLE, MI_PATH )
    f_ult.escribe_una_linea( texto )
    dbx.cargar( f_ult.en_path(), f_ult.en_raiz() )

def toot2tuit( id_toot, senal = '' ):
    # Publica un toot en Twitter pasado como id
    print( 'toot2tuit() => Publicar toot ', id_toot )
    cuerpo = masto.cuerpo( id_toot )
    if senal != '':
        cuerpo = cuerpo.replace( senal, '' ).strip()
    url_toot = tuiter._acortar_enlace( masto.enlace( id_toot ) )
    cuerpo = cuerpo + CADRT + url_toot
    tuiter.tuitear( cuerpo, adjuntos = masto.adjuntos( id_toot ) )

def hilo2tuit( id_toot, senal = '' ):
    # Publica en Twitter todos los toots por encima de uno pasado como id
    print( 'hilo2tuit() => Publicar hilo encima de ', id_toot )
    lista_tuits = []
    for id_lista in masto.hilo_hacia_arriba( id_toot ):
        cuerpo = masto.cuerpo( id_lista )
        if senal != '':
            cuerpo = cuerpo.replace( senal, '' ).strip()
        url_toot = tuiter._acortar_enlace( masto.enlace( id_lista ) )
        cuerpo = cuerpo + CADRT + url_toot
        tuit = { 'cuerpo' : cuerpo, 'adjuntos' : masto.adjuntos( id_lista ) }
        lista_tuits.append( tuit )
    tuiter.tuit_hilo( lista_tuits )

def buscar_id( id_toot ):
    # Busca la id de un toot a reprocesar dentro de otro toot
    url = masto.cuerpo( id_toot )
    url = url[ url.find( 'http' ) : len( url ) ]
    url = url[ 0 : url.find( ' ' ) ].strip()
    id_nuevo = masto.buscar_toot( url )
    return id_nuevo

def buscar_comando( masto, tuiter, dbx ):
    # Procesa los últimos toots en busca de los comandos de tuitear toot o hilo
    f_ult = mi_ficheros( EVITAR_DOBLE, MI_PATH )
    if dbx.descargar( f_ult.en_path(), f_ult.en_raiz() ):
        ultimo = f_ult.primera_linea()
    else:
        ultimo = None

    f_tuits = mi_ficheros( ULTIMOS_TOOTS, MI_PATH )
    if dbx.descargar( f_tuits.en_path(), f_tuits.en_raiz() ):
        ultimos = f_tuits.lee_lineas()

    toots = masto.ultimos_toots( desde = ultimo, max_toots = NUM_TOOTS )

    for id_toot in toots:
        if masto.buscar_tag( id_toot, SENAL_TUIT ):
            if not( str( id_toot ) in ultimos ):
                toot2tuit( id_toot, SENAL_TUIT )
                ultimos.append( id_toot )

        if masto.buscar_tag( id_toot, SENAL_HILO ):
            if not( str( id_toot ) in ultimos ):
                hilo2tuit( id_toot, SENAL_HILO )
                ultimos.append( id_toot )

        if masto.buscar_tag( id_toot, SENAL_RTOOT ):
            if not( str( id_toot ) in ultimos ):
                id_nuevo = buscar_id( id_toot )
                if id_nuevo != None:
                    toot2tuit( id_nuevo )
                else:
                    print( 'id_toot() => Toot ' + url + ' no encontrado' )
                ultimos.append( id_toot )

        if masto.buscar_tag( id_toot, SENAL_RHILO ):
            if not( str( id_toot ) in ultimos ):
                id_nuevo = buscar_id( id_toot )
                if id_nuevo != None:
                    hilo2tuit( id_nuevo )
                else:
                    print( 'id_toot() => Toot ' + url + ' no encontrado' )
                ultimos.append( id_toot )

    guarda_ultimos( ultimos, dbx )
    if len( toots ) > 0:
        guarda_el_ultimo( id_toot, dbx )

def retuitear( tuiter, id_tuit ):
    # Retuitea un tuit
    try:
        tuiter.retweet( id = id_tuit )
        return True
    except:
        return False

# MAIN
args = _args()
tuiter = mi_tuiter( args.twitter_api, args.twitter_sapi, args.twitter_token, args.twitter_stoken )
masto = mi_masto( args.masto_token, args.masto_inst )
dbx = mi_dropbox( args.dropbox_token )

if args.toot:
    tuitear_toot( masto, tuiter, args.toot )
elif args.hilo:
    hilo = tuitear_hilo( masto, tuiter, args.hilo )
else:
    buscar_comando( masto, tuiter, dbx )
