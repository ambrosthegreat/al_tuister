# al_tuister

Bot para hacer de pasarela desde Mastodon hacia Twitter funcionando en Heroku con el Heroku Scheduler.

Como Heroku no guarda los ficheros entre una y otra ejecución, es necesario almacenar los ficheros temporales en dropbox.

También se puede lanzar con parámetros para publicar en Twitter un determinado toot, aunque siempre que este bot esté en tu instancia, no he implementado la búsqueda del id en Mastodon.
