#!/usr/bin/env python3
#
# mi_mastodon.py
#
# Mi librería de acceso a Mastodon

# CONSTANTES
MASTODON_MAX = 500
MAX_TOOTS_LEER = 20

# LIBRERIAS
from html2text import HTML2Text
from lxml import html
from mastodon import Mastodon

# FUNCIONES
class mi_masto():
    def __init__( self, token, inst_url ):
        # Arranca la conexión con Mastodon
        self.masto = Mastodon( access_token = token, api_base_url = inst_url )
        self.verificar()

    def buscar_toot( self, url_toot ):
        # Devuelve el id interno de un toot (en principio, para buscarlo por la url)
        estado = self.masto.search_v2( url_toot )

        if len( estado.statuses ) != 1:
            print( 'mi_masto.toot_por_url() => No existe el estado ' + str( url_toot ) )
            return None
        else:
            id_toot = estado.statuses[0].id
            return id_toot
    
    def adjuntos( self, id_toot ):
        # Devulve la lista de adjuntos de un toot
        return self.masto.status( id_toot )['media_attachments']

    def buscar_tag( self, id_toot, mi_tag ):
        buscar = mi_tag.replace( '#', '' )
        mensaje = self.masto.status( id_toot )

        salida = False
        for tag_encontrado in mensaje['tags']:
            if tag_encontrado['name'] == buscar:
                salida = True
                break
        return salida

    def cuerpo( self, id_toot ):
        # Devuelve el cuerpo de un toot
        mensaje = self.masto.status( id_toot )['content'].strip()
        if mensaje != '':
            salida = self._html2texto( mensaje )
        else:
            salida = ''
        return salida

    def enlace( self, id_toot ):
        # Devuelve la url de un toot
        url = self.masto.status( id_toot )['url']
        return url

    def hilo_hacia_arriba( self, id_toot ):
        # Devuelve una lista de ids que se corresponden con los toots que hay por encima del que se pasa
        try:
            mi_toot = self.masto.status( id_toot )
        except:
            print( 'mi_masto.hilo_hacia_arriba => No existe el toot ' + str( id_toot ) )
            return None

        lista_toots = [ id_toot ]
        while mi_toot['in_reply_to_id']:
            mi_toot = self.masto.status( mi_toot['in_reply_to_id'] )
            lista_toots.append( mi_toot['id'] )

        salida = []
        for id_toot in reversed( lista_toots ):
            salida.append( id_toot )

        return salida

    """
    def _html2texto( self, doc_html ):
        # Convierte de html a texto
        doc = html.document_fromstring( doc_html )
        # Preserve end-of-lines
        # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
        for br in doc.xpath("*//br"):
            br.tail = "\n" + br.tail if br.tail else "\n"
        for p in doc.xpath("*//p"):
            p.tail = "\n" + p.tail if p.tail else "\n"
        return doc.text_content()
    """

    def _html2texto( self, cadena_html ):
        # Convierte en texto una cadena en html
        h = HTML2Text()
        h.body_width = 0
        h.ignore_links = True
        texto = h.handle( cadena_html ).strip()
        texto = texto.replace( '\-', '-' )
        texto = texto.replace( '\+', '+' )
        return texto

    def max_car( self ):
        # Devuelvo el número de caracteres máximo admitido en Mastodon
        return MASTODON_MAX

    def ultimos_toots( self, desde = None, max_toots = MAX_TOOTS_LEER ):
        # Devuelve los últimos toots de una cuenta
        toots = self.masto.account_statuses( self.usuario(), limit = max_toots, since_id = desde )
        print( 'mi_masto.ultimos_toots() => Leídos ' + str( len( toots ) ) + ' toots' )
        lista = []
        for toot in toots:
            lista.append( toot['id'] )

        return lista

    def usuario( self ):
        # Devuelve el id de la cuenta
        datos_usuario = self.masto.account_verify_credentials()
        usuario_id = datos_usuario['id']
        return usuario_id

    def verificar( self ):
        # Comprueba la conexión a Mastodon
        try:
            self.masto.account_verify_credentials()
            print( 'mi_masto.verificar() => Credenciales de Mastodon OK' )
            return True
        except:
            print( 'mi_masto.verificar() => ERROR: ', sys.exc_info()[0] )
            return False
