#!/usr/bin/env python3
#
# al_tuister.py
#
# Librería para leer mensajes de Mastodon y reenviarlos a Twitter
# Subo a dropbox los temporales
#
# Documentación: https://realpython.com/twitter-bot-python-tweepy/
# Subir imágenes desde una URL: https://teratail.com/questions/195943
# Mogollón de ejemplos: https://www.programcreek.com/python/example/6809/tweepy.API

# CONSTANTES
ACORTADOR = 'http://tinyurl.com/api-create.php?url='
CADRT = '\n\n🔗🐘: '
INSTANCIA = 'https://hispatodon.club/'
TOOT = 'https://hispatodon.club/@AmbrosTheGreat/104896342383584410'
TOOT2 = 'https://hispatodon.club/@AmbrosTheGreat/104896056003700550'
TOOT3 = 'https://hispatodon.club/@AmbrosTheGreat/104901487060350818'
TOOT4 = 'https://hispatodon.club/@AmbrosTheGreat/104914444866351690'

# LIBRERIAS
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from html2text import html2text
from html2text import HTML2Text
from mi_mastodon import mi_masto
from os import getenv as variable
from requests import get as url_get

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Tuitear cosas de Mastodon' )
    parser.add_argument( '-mi', '--masto_inst', help='Instancia de Mastodon', type = str )
    parser.add_argument( '-mt', '--masto_token', help='Token de la app de Mastodon', type = str )
    parser.add_argument( '-t', '--toot', help='Tuitea un toot pasado como URL', type = str )
    return parser.parse_args()

def _acortar_enlace( enlace ):
    # Acorta un enlace
    shortUrl = url_get( ACORTADOR + enlace )
    mi_URL =  shortUrl.content.decode( 'utf-8' )
    return mi_URL

def toot( cuerpo_en_html ):
    h = HTML2Text()
    h.body_width = 0
    h.ignore_links = True
    texto = h.handle( cuerpo_en_html )
    texto = texto.replace( '\-', '-' )
    texto = texto.replace( '\+', '+' )
    texto = texto.replace( '#tt', '' ).strip()
    return texto

def toot2tuit( cuerpo_html, enlace, senal = '#tt' ):
    cuerpo = toot( cuerpo_html )
    if senal != '':
        cuerpo = cuerpo.replace( senal, '' ).strip()
    url_toot = _acortar_enlace( enlace )
    cuerpo = cuerpo + CADRT + url_toot
    print( cuerpo )

# MAIN
args = _args()

if args.masto_token:
    masto_token = args.masto_token
else:
    masto_token = variable( 'MASTO_TOKEN' )

if args.masto_inst:
    instancia = args.masto_inst
else:
    instancia = INSTANCIA

"""
masto = mi_masto( masto_token, instancia )
id_toot = masto.buscar_toot( TOOT4 )
mi_toot_html = masto.masto.status( id_toot )['content'].strip()
print( mi_toot_html )
print( '---' )
"""
# mi_toot_html = '<p>¿Vacunas caseras?</p><p>Ya están mas cerca los superhéroes y supervillanos con poderes autoinoculados.</p><p>Gotham nos espera.</p><p>El peligro de ensayar vacunas ‘caseras’ contra la covid-19<br /><a href="https://hipertextual.com/2020/09/peligro-ensayan-vacunas-caseras" rel="nofollow noopener noreferrer" target="_blank"><span class="invisible">https://</span><span class="ellipsis">hipertextual.com/2020/09/pelig</span><span class="invisible">ro-ensayan-vacunas-caseras</span></a></p><p><a href="https://hispatodon.club/tags/tt" class="mention hashtag" rel="tag">#<span>tt</span></a></p>'
mi_toot_html = '<p>- Así que recién contratado...<br />PRRRR<br />+ Hostia qué pedo...<br /> Test superado, ¡siguiente!<br />- ¿?<br />- El primer síntoma del covid es la perdida del olfato. He desarrollado este test infalible, pero no cualquiera está capacitado para meterse un cocido diario como desayuno<br />_ Dios qué peste</p><p><a href="https://hispatodon.club/tags/tt" class="mention hashtag" rel="tag">#<span>tt</span></a></p>'
toot2tuit( mi_toot_html, TOOT4 )
