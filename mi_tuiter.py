#!/usr/bin/env python3
#
# mi_tuiter.py
#
# Mi librería de acceso a Twitter

# CONSTANTES
ACORTADOR = 'http://tinyurl.com/api-create.php?url='
TWITTTER_MAX = 280

# LIBRERIAS
from io import BytesIO
from lxml import html
from magic import Magic
from requests import get as url_get
from sys import exc_info as errores
from twython import Twython

# FUNCIONES
class mi_tuiter():
    def __init__( self, api, secret_api, token, secret_token ):
        # Arranca la conexión con Twitter y verifica que es correcta
        self.twitter = Twython( api, secret_api, token, secret_token )
        self.verificar()

    def _acortar_enlace( self, enlace ):
        # Acorta un enlace
        shortUrl = url_get( ACORTADOR + enlace )
        mi_URL =  shortUrl.content.decode( 'utf-8' )
        return mi_URL

    def _acortar_texto( self, texto, maximo = TWITTTER_MAX ):
        # Devuelve la primera parte de una cadena de longitud inferior al máximo admitido por Twitter
        if len( texto ) < maximo:
            salida = texto
        else:
            salida = texto[ 0 : maximo ]
            c_final = len( salida )
            for c in reversed( salida ):
                if c == ' ' or c == '\t' or c == '\n':
                    c_final -= 1
                    break
                else:
                    c_final -= 1
            salida = salida[ 0 : c_final ]

        if len( salida ) == 0:
            salida = texto[ 0 : maximo ]

        return salida

    def _dividir( self, texto, maximo = TWITTTER_MAX ):
        # Divide una cadena para formar una lista de cadenas de longitud inferior al máximo admitido por Twitter
        salida = []
        if texto == '':
            salida.append( texto )
        else:
            conector = '\n\t[...]'
            queda = texto
    
            while len( queda ) > 0:
                mi_maximo = maximo - len( conector )
                if len( queda ) > maximo:
                    nuevo = self._acortar_texto( queda, mi_maximo )
                    queda = queda[ len( nuevo ) : len( queda ) ].strip()
                    nuevo = nuevo + conector
                    salida.append( nuevo )
                else:
                    salida.append( queda )
                    queda = ''

        return salida

    def formatear( self, texto, es_html = True ):
        # Formatea un texto y devuelve una lista de cadenas con la longitud adecuada y los enlaces acortados
        if es_html:
            mi_texto = self._html2texto( texto )

        mi_texto = self._reducir_enlaces( mi_texto )

        return mi_texto

    """
    def _html2texto( self, doc_html ):
        # Convierte de html a texto
        doc = html.document_fromstring( doc_html )
        # Preserve end-of-lines
        # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
        for br in doc.xpath("*//br"):
            br.tail = "\n" + br.tail if br.tail else "\n"
        for p in doc.xpath("*//p"):
            p.tail = "\n" + p.tail if p.tail else "\n"
        return doc.text_content()
    """

    def _html2texto( self, cadena_html ):
        # Convierte en texto una cadena en html
        h = HTML2Text()
        h.body_width = 0
        h.ignore_links = True
        texto = h.handle( cadena_html ).strip()
        texto = texto.replace( '\-', '-' )
        texto = texto.replace( '\+', '+' )
        return texto

    def _localizar_enlaces( self, texto ):
        # Busca todos los enlaces en un texto
        inicio = 0
        enlaces = []
        while texto.find( 'http', inicio ) >= 0:
            inicio = texto.find( 'http', inicio )
            cadena = texto[ inicio : len( texto ) ]
            inicio += 1
            cadena = cadena.split()[0]
            enlaces.append( cadena )
        return enlaces

    def max_car( self ):
        # Devuelvo el número de caracteres máximo admitido en Twitter
        return TWITTTER_MAX
    
    def _reducir_enlaces( self, texto ):
        # Acorta todos los enlaces de un texto
        enlaces = self._localizar_enlaces( texto )
        salida = texto
        for enlace in enlaces:
            salida = salida.replace( enlace, self._acortar_enlace( enlace ) )
        return salida

    def _subir_adjuntos( self, lista = [] ):
        # Sube a Twitter una lista de adjuntos
        lista_imagenes = []

        for imagen in lista:
            mi_url = imagen['url']
            nombre = mi_url.split( '/' )[-1]
            descargada = url_get( mi_url ).content

            mi_magic = Magic( mime = True )
            tipo_mime = mi_magic.from_buffer( descargada )

            print( 'mi_tuiter.subir_adjuntos() => Subir ', nombre )
            # 'type': # Media type: 'image', 'video', 'gifv', 'audio' or 'unknown'.
            try:
                if tipo_mime.split( '/' )[0] == 'image':
                    subido = self.twitter.upload_media( media=BytesIO( descargada ) )
                    lista_imagenes.append( subido['media_id'] )
                elif tipo_mime.split( '/' )[0] == 'video':
                    subido = self.twitter.upload_video( media=BytesIO( descargada ), media_type=tipo_mime )
                    lista_imagenes.append( subido['media_id'] )
            except:
                print( 'mi_tuiter.subir_adjuntos() => ERROR: ', errores()[0] )
            
            if len( lista_imagenes ) == 0:
                lista_imagenes = None

        return lista_imagenes   

    def tuitear( self, cuerpo = '', en_respuesta_a = None, adjuntos = [], maximo = TWITTTER_MAX ):
        # Escribe un tuit
        imagenes = self._subir_adjuntos( adjuntos )
        lista_tuits = self._dividir( cuerpo, maximo )
        
        resp_id = en_respuesta_a
        for texto in lista_tuits:
            mi_tuit = self.twitter.update_status( status = texto, media_ids = imagenes, in_reply_to_status_id = resp_id )
            imagenes = None     # Sólo subo una vez los adjuntos
            print( 'mi_tuiter.tuitear() => Tuiteado \"' + texto + '\" en respuesta a ' + str( resp_id ) )
            resp_id = mi_tuit['id_str']

        return lista_tuits

    def tuit_de_fichero( self, nom_fich, en_respuesta_a = None, adjuntos = [], maximo = TWITTTER_MAX ):
        # Escribe un tuit a partir de un fichero de texto
        f = open( nom_fich )
        texto = f.read()
        f.close()

        lista = self.tuitear( texto, en_respuesta_a, adjuntos, maximo )
        return lista

    def tuit_hilo( self, hilo, en_respuesta_a = None, maximo = TWITTTER_MAX ):
        # Tuitea un hilo que se pasa como una lista de cadenas de tuits
        # Cada tuit tiene estos campos: cuerpo, adjuntos
        lista = []
        anterior = en_respuesta_a
        for mensaje in hilo:
            imagenes = self._subir_adjuntos( mensaje['adjuntos'] )
            lista_mensaje = self._dividir( mensaje['cuerpo'], maximo )

            for texto in lista_mensaje:
                 mi_tuit = self.twitter.update_status( status = texto, media_ids = imagenes, in_reply_to_status_id = anterior )
                 imagenes = None
                 print( 'mi_tuiter.tuit_hilo() => Tuiteado \"' + texto + '\" en respuesta a ' + str( anterior ) )
                 anterior = mi_tuit['id_str']
                 lista.append( texto )
        
        return lista

    def verificar( self ):
        # Comprueba la conexión a Twitter
        try:
            self.twitter.verify_credentials()
            print( 'mi_tuiter.verificar() => Credenciales de Twitter OK' )
            return True
        except:
            print( 'mi_tuiter.verificar() => ERROR: ', errores()[0] )
            return False
