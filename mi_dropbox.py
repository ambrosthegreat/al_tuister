#!/usr/bin/env python3
#
# mi_dropbox
# Librería para interactuar con dropbox
#

# CONSTANTES

# LIBRERIAS
from dropbox import Dropbox
from dropbox.files import WriteMode

# FUNCIONES
class mi_dropbox():
    def __init__( self, dbx_token ):
        self.dbx = Dropbox( dbx_token )

    def descargar( self, nom_local, nom_dbx ):
        # Descarga el fichero nom_dbx de dropbox a nom_local en local
        try:
            self.dbx.files_download_to_file( nom_local, nom_dbx )
            return True
        except:
            return False

    def cargar( self, nom_local, nom_dbx ):
        # Carga el fichero nom_local de local al fichero nom_dbx en dropbox
        try:
            self.dbx.files_upload( open( nom_local, 'rb' ).read(), nom_dbx, WriteMode( 'overwrite', None ) )
            return True
        except:
            return False
